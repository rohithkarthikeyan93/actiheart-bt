// actiheart-winsocket.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <winsock2.h>
#include <ws2bth.h>

SOCKADDR_BTH sockAddr;
SOCKET btSocket;

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

#pragma comment(lib, "Ws2_32.lib")


int main()
{

    int iResult;
    int error;

    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    WSADATA wsaData;


    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != NO_ERROR) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    std::cout << "I'm trying\n";
    btSocket = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);
    memset(&sockAddr, 0, sizeof(sockAddr));
    sockAddr.addressFamily = AF_BTH;
    sockAddr.serviceClassId = RFCOMM_PROTOCOL_UUID;
    sockAddr.port = BT_PORT_ANY;
    sockAddr.btAddr = 0x60c0bf0daa83;

    error = connect(btSocket, (SOCKADDR*)&sockAddr, sizeof(sockAddr));
    do {

        iResult = recv(btSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0)
            printf("Bytes received: %d\n", iResult);
        else if (iResult == 0)
            printf("Connection closed\n");
        else
            printf("recv failed: %d\n", WSAGetLastError());

    } while (iResult > 0);

}